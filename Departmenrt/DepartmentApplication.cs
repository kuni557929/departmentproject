﻿using Departmenrt.Data;
using Departmenrt.Domain.Interactors;
using Departmenrt.Presentation.Authorization;
using System;
using System.Windows.Forms;

namespace Departmenrt
{
    internal static class DepartmentApplication
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new AuthorizationView());
        }
    }
}
