﻿using Departmenrt.Presentation.Authorization;
using Departmenrt.Presentation.View.Authorization;
using System;
using System.Windows.Forms;

namespace Departmenrt
{
    /// <summary>
    /// Класс является основным классов для AuthorizationView. Реализует <see cref="IAuthorizationView"/>
    /// </summary>
    public partial class AuthorizationView : Form, IAuthorizationView
    {
        private readonly AuthorizationPresenter presenter;

        public AuthorizationView()
        {
            InitializeComponent();

            this.presenter = new AuthorizationPresenter(this);
        }

        public void ExciteErrorProvider(string errorField, string message)
        {
            if (errorField == "login")
            {
                m_EP_login.SetError(m_TB_login, message);
            }
            else if (errorField == "password")
            {
                m_EP_password.SetError(m_TB_password, message);
            }
        }

        public void ResetErrorProvider()
        {
            m_EP_login.Clear();
            m_EP_password.Clear();
        }

        public void ResetFields()
        {
            m_TB_login.Clear();
            m_TB_password.Clear();
        }

        public new void Hide()
        {
            this.ResetErrorProvider();
            this.ResetFields();
            base.Hide();
        }

        private void m_BTN_authorization_Click(object sender, EventArgs e)
        {
            presenter.Authorization(m_TB_login.Text, m_TB_password.Text);
        }
    }
}
