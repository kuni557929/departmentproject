﻿namespace Departmenrt
{
    partial class AuthorizationView
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.m_BTN_authorization = new System.Windows.Forms.Button();
            this.m_L_password = new System.Windows.Forms.Label();
            this.m_L_login = new System.Windows.Forms.Label();
            this.m_TB_password = new System.Windows.Forms.TextBox();
            this.m_TB_login = new System.Windows.Forms.TextBox();
            this.m_l_label = new System.Windows.Forms.Label();
            this.m_EP_login = new System.Windows.Forms.ErrorProvider(this.components);
            this.m_EP_password = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_EP_login)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_EP_password)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.m_BTN_authorization);
            this.panel1.Controls.Add(this.m_L_password);
            this.panel1.Controls.Add(this.m_L_login);
            this.panel1.Controls.Add(this.m_TB_password);
            this.panel1.Controls.Add(this.m_TB_login);
            this.panel1.Location = new System.Drawing.Point(179, 134);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(402, 260);
            this.panel1.TabIndex = 0;
            // 
            // m_BTN_authorization
            // 
            this.m_BTN_authorization.Location = new System.Drawing.Point(133, 179);
            this.m_BTN_authorization.Name = "m_BTN_authorization";
            this.m_BTN_authorization.Size = new System.Drawing.Size(115, 47);
            this.m_BTN_authorization.TabIndex = 4;
            this.m_BTN_authorization.Text = "Войти";
            this.m_BTN_authorization.UseVisualStyleBackColor = true;
            this.m_BTN_authorization.Click += new System.EventHandler(this.m_BTN_authorization_Click);
            // 
            // m_L_password
            // 
            this.m_L_password.AutoSize = true;
            this.m_L_password.Location = new System.Drawing.Point(53, 122);
            this.m_L_password.Name = "m_L_password";
            this.m_L_password.Size = new System.Drawing.Size(45, 13);
            this.m_L_password.TabIndex = 3;
            this.m_L_password.Text = "Пароль";
            // 
            // m_L_login
            // 
            this.m_L_login.AutoSize = true;
            this.m_L_login.Location = new System.Drawing.Point(53, 50);
            this.m_L_login.Name = "m_L_login";
            this.m_L_login.Size = new System.Drawing.Size(38, 13);
            this.m_L_login.TabIndex = 2;
            this.m_L_login.Text = "Логин";
            // 
            // m_TB_password
            // 
            this.m_TB_password.Location = new System.Drawing.Point(53, 138);
            this.m_TB_password.Name = "m_TB_password";
            this.m_TB_password.PasswordChar = '*';
            this.m_TB_password.Size = new System.Drawing.Size(288, 20);
            this.m_TB_password.TabIndex = 1;
            // 
            // m_TB_login
            // 
            this.m_TB_login.Location = new System.Drawing.Point(53, 69);
            this.m_TB_login.Name = "m_TB_login";
            this.m_TB_login.Size = new System.Drawing.Size(288, 20);
            this.m_TB_login.TabIndex = 0;
            // 
            // m_l_label
            // 
            this.m_l_label.AutoSize = true;
            this.m_l_label.BackColor = System.Drawing.Color.Red;
            this.m_l_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.m_l_label.Location = new System.Drawing.Point(273, 58);
            this.m_l_label.Name = "m_l_label";
            this.m_l_label.Size = new System.Drawing.Size(218, 73);
            this.m_l_label.TabIndex = 10;
            this.m_l_label.Text = "ВХОД";
            // 
            // m_EP_login
            // 
            this.m_EP_login.ContainerControl = this;
            // 
            // m_EP_password
            // 
            this.m_EP_password.ContainerControl = this;
            // 
            // AuthorizationView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.m_l_label);
            this.Controls.Add(this.panel1);
            this.MaximumSize = new System.Drawing.Size(816, 489);
            this.MinimumSize = new System.Drawing.Size(816, 489);
            this.Name = "AuthorizationView";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_EP_login)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_EP_password)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button m_BTN_authorization;
        private System.Windows.Forms.Label m_L_password;
        private System.Windows.Forms.Label m_L_login;
        private System.Windows.Forms.TextBox m_TB_password;
        private System.Windows.Forms.TextBox m_TB_login;
        private System.Windows.Forms.Label m_l_label;
        private System.Windows.Forms.ErrorProvider m_EP_login;
        private System.Windows.Forms.ErrorProvider m_EP_password;
    }
}

