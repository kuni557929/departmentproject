﻿namespace Departmenrt.Presentation.View.Authorization
{
    /// <summary>
    /// Интерфейс, предоставляющий основные методы для работы с элементами пользовательского интерфейса
    /// </summary>
    public interface IAuthorizationView
    {
        /// <summary>
        /// Метод возбуждает ошибку определенного поля по переданному названию с переданным сообщением
        /// </summary>
        /// <param name="errorField">Название поля, по которому будет возбуждаться ошибка</param>
        /// <param name="message">Сообщение, которое будет показываться пользователю</param>
        void ExciteErrorProvider(string errorField, string message);

        /// <summary>
        /// Метод используется для сброса всех возбужденных ошибок
        /// </summary>
        void ResetErrorProvider();

        /// <summary>
        /// Метод используется для сброса значений всех компонентов пользовательского интерфейса
        /// </summary>
        void ResetFields();

        /// <summary>
        /// Метод используется для сокрытия формы
        /// </summary>
        void Hide();
    }
}
