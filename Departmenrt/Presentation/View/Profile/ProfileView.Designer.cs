﻿namespace Departmenrt.Presentation.View.Profile
{
    partial class ProfileView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_PB_avatar = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.m_L_id = new System.Windows.Forms.Label();
            this.m_TB_id = new System.Windows.Forms.TextBox();
            this.m_L_role = new System.Windows.Forms.Label();
            this.m_L_login = new System.Windows.Forms.Label();
            this.m_TB_role = new System.Windows.Forms.TextBox();
            this.m_TB_login = new System.Windows.Forms.TextBox();
            this.m_BTN_add_image = new System.Windows.Forms.Button();
            this.m_BTN_prev = new System.Windows.Forms.Button();
            this.m_BTN_next = new System.Windows.Forms.Button();
            this.m_BTN_next_user = new System.Windows.Forms.Button();
            this.m_BTN_prev_user = new System.Windows.Forms.Button();
            this.m_L_number = new System.Windows.Forms.Label();
            this.m_TB_number = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.m_PB_avatar)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_PB_avatar
            // 
            this.m_PB_avatar.Image = global::Departmenrt.Properties.Resources.Image;
            this.m_PB_avatar.Location = new System.Drawing.Point(301, 13);
            this.m_PB_avatar.Name = "m_PB_avatar";
            this.m_PB_avatar.Size = new System.Drawing.Size(199, 182);
            this.m_PB_avatar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.m_PB_avatar.TabIndex = 4;
            this.m_PB_avatar.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.m_L_number);
            this.panel1.Controls.Add(this.m_TB_number);
            this.panel1.Controls.Add(this.m_L_id);
            this.panel1.Controls.Add(this.m_TB_id);
            this.panel1.Controls.Add(this.m_L_role);
            this.panel1.Controls.Add(this.m_L_login);
            this.panel1.Controls.Add(this.m_TB_role);
            this.panel1.Controls.Add(this.m_TB_login);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(282, 245);
            this.panel1.TabIndex = 5;
            // 
            // m_L_id
            // 
            this.m_L_id.AutoSize = true;
            this.m_L_id.Location = new System.Drawing.Point(9, 10);
            this.m_L_id.Name = "m_L_id";
            this.m_L_id.Size = new System.Drawing.Size(16, 13);
            this.m_L_id.TabIndex = 9;
            this.m_L_id.Text = "Id";
            // 
            // m_TB_id
            // 
            this.m_TB_id.Location = new System.Drawing.Point(12, 26);
            this.m_TB_id.Name = "m_TB_id";
            this.m_TB_id.ReadOnly = true;
            this.m_TB_id.Size = new System.Drawing.Size(252, 20);
            this.m_TB_id.TabIndex = 8;
            // 
            // m_L_role
            // 
            this.m_L_role.AutoSize = true;
            this.m_L_role.Location = new System.Drawing.Point(9, 110);
            this.m_L_role.Name = "m_L_role";
            this.m_L_role.Size = new System.Drawing.Size(32, 13);
            this.m_L_role.TabIndex = 7;
            this.m_L_role.Text = "Роль";
            // 
            // m_L_login
            // 
            this.m_L_login.AutoSize = true;
            this.m_L_login.Location = new System.Drawing.Point(9, 61);
            this.m_L_login.Name = "m_L_login";
            this.m_L_login.Size = new System.Drawing.Size(29, 13);
            this.m_L_login.TabIndex = 6;
            this.m_L_login.Text = "Имя";
            // 
            // m_TB_role
            // 
            this.m_TB_role.Location = new System.Drawing.Point(12, 126);
            this.m_TB_role.Name = "m_TB_role";
            this.m_TB_role.ReadOnly = true;
            this.m_TB_role.Size = new System.Drawing.Size(252, 20);
            this.m_TB_role.TabIndex = 5;
            // 
            // m_TB_login
            // 
            this.m_TB_login.Location = new System.Drawing.Point(12, 77);
            this.m_TB_login.Name = "m_TB_login";
            this.m_TB_login.ReadOnly = true;
            this.m_TB_login.Size = new System.Drawing.Size(252, 20);
            this.m_TB_login.TabIndex = 4;
            // 
            // m_BTN_add_image
            // 
            this.m_BTN_add_image.Location = new System.Drawing.Point(300, 229);
            this.m_BTN_add_image.Name = "m_BTN_add_image";
            this.m_BTN_add_image.Size = new System.Drawing.Size(200, 25);
            this.m_BTN_add_image.TabIndex = 6;
            this.m_BTN_add_image.Text = "Загрузить изображение";
            this.m_BTN_add_image.UseVisualStyleBackColor = true;
            this.m_BTN_add_image.Click += new System.EventHandler(this.m_BTN_add_image_Click);
            // 
            // m_BTN_prev
            // 
            this.m_BTN_prev.Location = new System.Drawing.Point(301, 201);
            this.m_BTN_prev.Name = "m_BTN_prev";
            this.m_BTN_prev.Size = new System.Drawing.Size(100, 23);
            this.m_BTN_prev.TabIndex = 7;
            this.m_BTN_prev.Text = "Назад";
            this.m_BTN_prev.UseVisualStyleBackColor = true;
            this.m_BTN_prev.Click += new System.EventHandler(this.m_BTN_prev_Click);
            // 
            // m_BTN_next
            // 
            this.m_BTN_next.Location = new System.Drawing.Point(400, 201);
            this.m_BTN_next.Name = "m_BTN_next";
            this.m_BTN_next.Size = new System.Drawing.Size(100, 23);
            this.m_BTN_next.TabIndex = 8;
            this.m_BTN_next.Text = "Вперед";
            this.m_BTN_next.UseVisualStyleBackColor = true;
            this.m_BTN_next.Click += new System.EventHandler(this.m_BTN_next_Click);
            // 
            // m_BTN_next_user
            // 
            this.m_BTN_next_user.Location = new System.Drawing.Point(400, 260);
            this.m_BTN_next_user.Name = "m_BTN_next_user";
            this.m_BTN_next_user.Size = new System.Drawing.Size(100, 23);
            this.m_BTN_next_user.TabIndex = 10;
            this.m_BTN_next_user.Text = ">>";
            this.m_BTN_next_user.UseVisualStyleBackColor = true;
            this.m_BTN_next_user.Click += new System.EventHandler(this.m_BTN_next_user_Click);
            // 
            // m_BTN_prev_user
            // 
            this.m_BTN_prev_user.Location = new System.Drawing.Point(301, 260);
            this.m_BTN_prev_user.Name = "m_BTN_prev_user";
            this.m_BTN_prev_user.Size = new System.Drawing.Size(100, 23);
            this.m_BTN_prev_user.TabIndex = 9;
            this.m_BTN_prev_user.Text = "<<";
            this.m_BTN_prev_user.UseVisualStyleBackColor = true;
            this.m_BTN_prev_user.Click += new System.EventHandler(this.m_BTN_prev_user_Click);
            // 
            // m_L_number
            // 
            this.m_L_number.AutoSize = true;
            this.m_L_number.Location = new System.Drawing.Point(9, 161);
            this.m_L_number.Name = "m_L_number";
            this.m_L_number.Size = new System.Drawing.Size(41, 13);
            this.m_L_number.TabIndex = 11;
            this.m_L_number.Text = "Номер";
            // 
            // m_TB_number
            // 
            this.m_TB_number.Location = new System.Drawing.Point(12, 177);
            this.m_TB_number.Name = "m_TB_number";
            this.m_TB_number.ReadOnly = true;
            this.m_TB_number.Size = new System.Drawing.Size(252, 20);
            this.m_TB_number.TabIndex = 10;
            // 
            // ProfileView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(527, 285);
            this.Controls.Add(this.m_BTN_next_user);
            this.Controls.Add(this.m_BTN_prev_user);
            this.Controls.Add(this.m_BTN_next);
            this.Controls.Add(this.m_BTN_prev);
            this.Controls.Add(this.m_BTN_add_image);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.m_PB_avatar);
            this.MaximumSize = new System.Drawing.Size(543, 324);
            this.MinimumSize = new System.Drawing.Size(543, 324);
            this.Name = "ProfileView";
            this.Text = "ProfileView";
            ((System.ComponentModel.ISupportInitialize)(this.m_PB_avatar)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox m_PB_avatar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label m_L_role;
        private System.Windows.Forms.Label m_L_login;
        private System.Windows.Forms.TextBox m_TB_role;
        private System.Windows.Forms.TextBox m_TB_login;
        private System.Windows.Forms.Button m_BTN_add_image;
        private System.Windows.Forms.Button m_BTN_prev;
        private System.Windows.Forms.Button m_BTN_next;
        private System.Windows.Forms.Label m_L_id;
        private System.Windows.Forms.TextBox m_TB_id;
        private System.Windows.Forms.Button m_BTN_next_user;
        private System.Windows.Forms.Button m_BTN_prev_user;
        private System.Windows.Forms.Label m_L_number;
        private System.Windows.Forms.TextBox m_TB_number;
    }
}