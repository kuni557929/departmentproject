﻿using System.Drawing;
using System.Windows.Forms;

namespace Departmenrt.Presentation.View.Profile
{
    /// <summary>
    /// Интерфейс, предоставляющий основные методы для работы с элементами пользовательского интерфейса
    /// </summary>
    public interface IProfileView
    {
        /// <summary>
        /// Метод используется для установки переданного значения в поле пользовательского интерфейса, которое показывает логин
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        void SetLogin(string login);

        /// <summary>
        /// Метод используется для установки переданного значения в поле пользовательского интерфейса, которое показывает роль
        /// </summary>
        /// <param name="role">Роль пользователя</param>
        void SetRole(string role);

        /// <summary>
        /// Метод используется для установки переданного значения в поле пользовательского интерфейса, которое показывает id пользователя
        /// </summary>
        /// <param name="userId">Id пользователя</param>
        void SetUserId(int userId);

        /// <summary>
        /// Метод используется для установки фотографии пользователя
        /// </summary>
        /// <param name="bitmap">Фотография пользователя</param>
        void SetAvatar(Bitmap bitmap);

        /// <summary>
        /// Устанавливает номер пользователя на форму
        /// </summary>
        /// <param name="number">Номер пользователя</param>
        void SetNumber(long number);

        /// <summary>
        /// Возвращает кнопку "Вперед" для перелистывания картинок
        /// </summary>
        /// <returns>Кнопку</returns>
        Control GetImageNextButton();

        /// <summary>
        /// Возвращает кнопку "Назад" для перелистывания картинок
        /// </summary>
        /// <returns>Кнопку</returns>
        Control GetImagePrevButton();

        /// <summary>
        /// Возвращает кнопку "Вперед" для перелистывания пользователей
        /// </summary>
        /// <returns>Кнопку</returns>
        Control GetUserNextButton();

        /// <summary>
        /// Возвращает кнопку "Назад" для перелистывания пользователей
        /// </summary>
        /// <returns>Кнопку</returns>
        Control GetUserPrevButton();

        /// <summary>
        /// Возвращает кнопку "Добавить картинку"
        /// </summary>
        /// <returns>Кнопку</returns>
        Control GetAddImageButton();
    }
}
