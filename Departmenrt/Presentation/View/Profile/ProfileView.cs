﻿using Departmenrt.Data.Enums;
using Departmenrt.Presentation.Presenter;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Departmenrt.Presentation.View.Profile
{
    /// <summary>
    /// Основной класс по работе с ProfileView. Реализация <see cref="IProfileView"/>
    /// </summary>
    public partial class ProfileView : Form, IProfileView
    {
        private readonly ProfilePresentation profilePresentation;

        public ProfileView(string login)
        {
            InitializeComponent();

            this.profilePresentation = new ProfilePresentation(this);
            this.profilePresentation.Initialize(login);
        }

        public void SetLogin(string login)
        {
            m_TB_login.Text = login;
        }

        public void SetRole(string role)
        {
            m_TB_role.Text = role;
        }

        public void SetUserId(int userId)
        {
            m_TB_id.Text = userId.ToString();
        }

        public void SetAvatar(Bitmap bitmap)
        {
            m_PB_avatar.Image = bitmap;
        }

        public void SetNumber(long number)
        {
            m_TB_number.Text = number.ToString();
        }

        public Control GetImageNextButton()
        {
            return m_BTN_next;
        }

        public Control GetImagePrevButton()
        {
            return m_BTN_prev;
        }

        public Control GetUserNextButton()
        {
            return m_BTN_next_user;
        }

        public Control GetUserPrevButton()
        {
            return m_BTN_prev_user;
        }

        public Control GetAddImageButton()
        {
            return m_BTN_add_image;
        }

        private void m_BTN_add_image_Click(object sender, EventArgs e)
        {
            profilePresentation.AddImage();
        }

        private void m_BTN_next_Click(object sender, EventArgs e)
        {
            profilePresentation.ChangeImage(Direction.NEXT);
        }

        private void m_BTN_prev_Click(object sender, EventArgs e)
        {
            profilePresentation.ChangeImage(Direction.PREV);
        }

        private void m_BTN_next_user_Click(object sender, EventArgs e)
        {
            profilePresentation.ChangeUser(Direction.NEXT);
        }

        private void m_BTN_prev_user_Click(object sender, EventArgs e)
        {
            profilePresentation.ChangeUser(Direction.PREV);
        }
    }
}