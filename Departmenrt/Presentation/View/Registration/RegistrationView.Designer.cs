﻿namespace Departmenrt.Presentation.Registration
{
    partial class RegistrationView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.m_TB_confirmPassword = new System.Windows.Forms.TextBox();
            this.m_TB_password = new System.Windows.Forms.TextBox();
            this.m_TB_login = new System.Windows.Forms.TextBox();
            this.m_L_repeat_password = new System.Windows.Forms.Label();
            this.m_L_password = new System.Windows.Forms.Label();
            this.m_L_login = new System.Windows.Forms.Label();
            this.m_BTN_login = new System.Windows.Forms.Button();
            this.m_BTN_registration = new System.Windows.Forms.Button();
            this.m_l_label = new System.Windows.Forms.Label();
            this.m_EP_login = new System.Windows.Forms.ErrorProvider(this.components);
            this.m_EP_password = new System.Windows.Forms.ErrorProvider(this.components);
            this.m_EP_confirmPassword = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_EP_login)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_EP_password)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_EP_confirmPassword)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.m_TB_confirmPassword);
            this.panel1.Controls.Add(this.m_TB_password);
            this.panel1.Controls.Add(this.m_TB_login);
            this.panel1.Controls.Add(this.m_L_repeat_password);
            this.panel1.Controls.Add(this.m_L_password);
            this.panel1.Controls.Add(this.m_L_login);
            this.panel1.Controls.Add(this.m_BTN_login);
            this.panel1.Controls.Add(this.m_BTN_registration);
            this.panel1.Location = new System.Drawing.Point(134, 106);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(511, 302);
            this.panel1.TabIndex = 8;
            // 
            // m_TB_confirmPassword
            // 
            this.m_TB_confirmPassword.Location = new System.Drawing.Point(135, 168);
            this.m_TB_confirmPassword.Name = "m_TB_confirmPassword";
            this.m_TB_confirmPassword.PasswordChar = '*';
            this.m_TB_confirmPassword.Size = new System.Drawing.Size(263, 20);
            this.m_TB_confirmPassword.TabIndex = 15;
            // 
            // m_TB_password
            // 
            this.m_TB_password.Location = new System.Drawing.Point(135, 110);
            this.m_TB_password.Name = "m_TB_password";
            this.m_TB_password.PasswordChar = '*';
            this.m_TB_password.Size = new System.Drawing.Size(263, 20);
            this.m_TB_password.TabIndex = 14;
            // 
            // m_TB_login
            // 
            this.m_TB_login.Location = new System.Drawing.Point(138, 49);
            this.m_TB_login.Name = "m_TB_login";
            this.m_TB_login.Size = new System.Drawing.Size(260, 20);
            this.m_TB_login.TabIndex = 13;
            // 
            // m_L_repeat_password
            // 
            this.m_L_repeat_password.AutoSize = true;
            this.m_L_repeat_password.Location = new System.Drawing.Point(135, 152);
            this.m_L_repeat_password.Name = "m_L_repeat_password";
            this.m_L_repeat_password.Size = new System.Drawing.Size(100, 13);
            this.m_L_repeat_password.TabIndex = 12;
            this.m_L_repeat_password.Text = "Повторите пароль";
            // 
            // m_L_password
            // 
            this.m_L_password.AutoSize = true;
            this.m_L_password.Location = new System.Drawing.Point(135, 94);
            this.m_L_password.Name = "m_L_password";
            this.m_L_password.Size = new System.Drawing.Size(45, 13);
            this.m_L_password.TabIndex = 11;
            this.m_L_password.Text = "Пароль";
            // 
            // m_L_login
            // 
            this.m_L_login.AutoSize = true;
            this.m_L_login.Location = new System.Drawing.Point(135, 33);
            this.m_L_login.Name = "m_L_login";
            this.m_L_login.Size = new System.Drawing.Size(38, 13);
            this.m_L_login.TabIndex = 10;
            this.m_L_login.Text = "Логин";
            // 
            // m_BTN_login
            // 
            this.m_BTN_login.Location = new System.Drawing.Point(305, 228);
            this.m_BTN_login.Name = "m_BTN_login";
            this.m_BTN_login.Size = new System.Drawing.Size(93, 40);
            this.m_BTN_login.TabIndex = 9;
            this.m_BTN_login.Text = "Войти";
            this.m_BTN_login.UseVisualStyleBackColor = true;
            this.m_BTN_login.Click += new System.EventHandler(this.m_BTN_login_Click);
            // 
            // m_BTN_registration
            // 
            this.m_BTN_registration.Location = new System.Drawing.Point(135, 228);
            this.m_BTN_registration.Name = "m_BTN_registration";
            this.m_BTN_registration.Size = new System.Drawing.Size(136, 40);
            this.m_BTN_registration.TabIndex = 8;
            this.m_BTN_registration.Text = "Зарегистрироваться";
            this.m_BTN_registration.UseVisualStyleBackColor = true;
            this.m_BTN_registration.Click += new System.EventHandler(this.m_BTN_registration_Click);
            // 
            // m_l_label
            // 
            this.m_l_label.AutoSize = true;
            this.m_l_label.BackColor = System.Drawing.Color.Red;
            this.m_l_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.m_l_label.Location = new System.Drawing.Point(134, 30);
            this.m_l_label.Name = "m_l_label";
            this.m_l_label.Size = new System.Drawing.Size(511, 73);
            this.m_l_label.TabIndex = 9;
            this.m_l_label.Text = "РЕГИСТРАЦИЯ";
            // 
            // m_EP_login
            // 
            this.m_EP_login.ContainerControl = this;
            // 
            // m_EP_password
            // 
            this.m_EP_password.ContainerControl = this;
            // 
            // m_EP_confirmPassword
            // 
            this.m_EP_confirmPassword.ContainerControl = this;
            // 
            // RegistrationView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.m_l_label);
            this.Controls.Add(this.panel1);
            this.MaximumSize = new System.Drawing.Size(816, 489);
            this.MinimumSize = new System.Drawing.Size(816, 489);
            this.Name = "RegistrationView";
            this.Text = "RegistrationView";
            this.Load += new System.EventHandler(this.RegistrationView_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_EP_login)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_EP_password)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_EP_confirmPassword)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox m_TB_confirmPassword;
        private System.Windows.Forms.TextBox m_TB_password;
        private System.Windows.Forms.TextBox m_TB_login;
        private System.Windows.Forms.Label m_L_repeat_password;
        private System.Windows.Forms.Label m_L_password;
        private System.Windows.Forms.Label m_L_login;
        private System.Windows.Forms.Button m_BTN_login;
        private System.Windows.Forms.Button m_BTN_registration;
        private System.Windows.Forms.Label m_l_label;
        private System.Windows.Forms.ErrorProvider m_EP_login;
        private System.Windows.Forms.ErrorProvider m_EP_password;
        private System.Windows.Forms.ErrorProvider m_EP_confirmPassword;
    }
}