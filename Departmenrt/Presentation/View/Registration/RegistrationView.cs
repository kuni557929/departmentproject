﻿using Departmenrt.Presentation.View.Registration;
using System;
using System.Windows.Forms;

namespace Departmenrt.Presentation.Registration
{
    /// <summary>
    /// Класс является основным классов для RegistrationView. Реализует <see cref="IRegistrationView"/>
    /// </summary>
    public partial class RegistrationView : Form, IRegistrationView
    {
        private readonly RegistrationPresenter presenter;

        public RegistrationView()
        {
            InitializeComponent();

            this.presenter = new RegistrationPresenter(this);
        }

        public void ExciteErrorProvider(string errorField, string message)
        {
            if (errorField == "login")
            {
                m_EP_login.SetError(m_TB_login, message);
            }
            else if (errorField == "password")
            {
                m_EP_password.SetError(m_TB_password, message);
            }
            else if (errorField == "confirmPassword")
            {
                m_EP_confirmPassword.SetError(m_TB_confirmPassword, message);
            }
        }

        public void ResetErrorProvider()
        {
            m_EP_login.Clear();
            m_EP_password.Clear();
            m_EP_confirmPassword.Clear();
        }

        public void ResetFields()
        {
            m_TB_login.Clear();
            m_TB_password.Clear();
            m_TB_confirmPassword.Clear();
        }

        public new void Hide()
        {
            this.ResetErrorProvider();
            this.ResetFields();

            base.Hide();
        }

        private void RegistrationView_Load(object sender, EventArgs e)
        {
            if (Owner is AuthorizationView)
            {
                FormClosing += (sen, ea) => ((AuthorizationView)Owner).Show();
            }
        }

        private void m_BTN_login_Click(object sender, EventArgs e)
        {
            if (Owner is AuthorizationView)
            {
                ((AuthorizationView)Owner).Show();
                Close();
            }
        }

        private void m_BTN_registration_Click(object sender, EventArgs e)
        {
            presenter.Registration(m_TB_login.Text, m_TB_password.Text, m_TB_confirmPassword.Text);
        }
    }
}
