﻿using Departmenrt.Domain.Exceptions;
using Departmenrt.Domain.Interactors;
using Departmenrt.Presentation.Presenter;
using System;

namespace Departmenrt.Presentation.Registration
{
    public class RegistrationPresenter : BasePresenter
    {
        private readonly RegistrationView view;
        private readonly RegistrationUseCase registrationUseCase;

        public RegistrationPresenter(RegistrationView view)
        {
            this.view = view;
            this.registrationUseCase = new RegistrationUseCase();
        }

        public void Registration(string login, string password, string confirmPassword)
        {
            try
            {
                view.ResetErrorProvider();

                var result = registrationUseCase.Execute(new RegistrationUseCase.Params(login, password, confirmPassword));

                if (result.Count == 0)
                {
                    ShowAuthorization();
                }
                else
                {
                    result.ForEach(tuple => view.ExciteErrorProvider(tuple.Item1, tuple.Item2));
                }

            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex.Message);
            }
        }

        public void ShowAuthorization()
        {
            view.Close();
        }

        public void HideView()
        {
            view.Hide();
        }
    }
}
