﻿using Departmenrt.Domain.Interactors;
using Departmenrt.Presentation.Presenter;
using Departmenrt.Presentation.Registration;
using Departmenrt.Presentation.View.Profile;
using System;
using System.Windows.Forms;

namespace Departmenrt.Presentation.Authorization
{
    public class AuthorizationPresenter : BasePresenter
    {

        private readonly AuthorizationUseCase authorizationUseCase;
        private readonly AuthorizationView view;

        public AuthorizationPresenter(AuthorizationView authorizationView)
        {
            this.view = authorizationView;
            this.authorizationUseCase = new AuthorizationUseCase();
        }

        public void Authorization(string login, string password)
        {
            try
            {
                view.ResetErrorProvider();

                var result = authorizationUseCase.Execute(new AuthorizationUseCase.Params(login, password));

                if (result.Count == 0)
                {
                    ShowProfile(login);
                }
                else
                {
                    result.ForEach(tuple => view.ExciteErrorProvider(tuple.Item1, tuple.Item2));
                }
            }
            catch (Exception ex)
            {
                DialogResult dialogResult = ShowYesNoErrorMessage(ex.Message);

                if (dialogResult == DialogResult.Yes)
                {
                    ShowRegistrationForm();
                }
            }
        }

        public void ShowRegistrationForm()
        {
            RegistrationView registrationView = new RegistrationView();
            registrationView.Show(view);

            HideView();
        }

        public void ShowProfile(string login)
        {
            ProfileView profileView = new ProfileView(login);
            profileView.Show();

            HideView();
        }

        public void HideView()
        {
            view.Hide();
        }
    }
}
