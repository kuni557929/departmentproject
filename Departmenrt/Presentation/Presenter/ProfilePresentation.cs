﻿using Departmenrt.Data.Enums;
using Departmenrt.Domain.Entity;
using Departmenrt.Domain.Interactors;
using Departmenrt.Presentation.View.Profile;
using Departmenrt.Properties;
using System.Drawing;
using System.Windows.Forms;

namespace Departmenrt.Presentation.Presenter
{
    public class ProfilePresentation : BasePresenter
    {
        public const string DIALOG_FILTER = "Bitmap Image (.bmp)|*.bmp|Gif Image (.gif)|*.gif|JPEG Image (.jpeg)|*.jpeg|Png Image (.png)|*.png|Tiff Image (.tiff)|*.tiff|Wmf Image (.wmf)|*.wmf";

        private readonly GetUserByNumber getUserByNumber;
        private readonly ConvertImageToBitMapUseCase convertImageToBitMapUseCase;
        private readonly GetFirstImageByUserIdUseCase getFirstImageByUserIdUseCase;
        private readonly GetImageByDirectionAndUserIdUseCase getImageByNumberAndUserIdUseCase;
        private readonly GetUserByLoginUseCase getUserByLoginUseCase;
        private readonly SaveImageUseCase saveImageUseCase;
        private readonly ProfileView view;

        private int mainUserId;
        private UserEntity userEntity;
        private ImageEntity imageEntity;

        public ProfilePresentation(ProfileView view)
        {
            this.view = view;
            getUserByLoginUseCase = new GetUserByLoginUseCase();
            saveImageUseCase = new SaveImageUseCase();
            getImageByNumberAndUserIdUseCase = new GetImageByDirectionAndUserIdUseCase();
            getFirstImageByUserIdUseCase = new GetFirstImageByUserIdUseCase();
            convertImageToBitMapUseCase = new ConvertImageToBitMapUseCase();
            getUserByNumber = new GetUserByNumber();
        }

        public void Initialize(string login)
        {
            this.userEntity = getUserByLoginUseCase.Execute(new GetUserByLoginUseCase.Params(login));
            this.mainUserId = this.userEntity.Id;

            FillForm();
        }

        public void ChangeUser(Direction direction)
        {
            this.userEntity = getUserByNumber.Execute(new GetUserByNumber.Params(userEntity, direction));

            FillForm();
        }

        public void AddImage()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = DIALOG_FILTER;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                this.imageEntity = saveImageUseCase.Execute(new SaveImageUseCase.Params(dialog.FileName, userEntity.Id));

                UpdateAvatar();
            }
        }

        public void ChangeImage(Direction direction)
        {
            if (imageEntity != null)
            {
                this.imageEntity = getImageByNumberAndUserIdUseCase.Execute(new GetImageByDirectionAndUserIdUseCase.Params(userEntity.Id, this.imageEntity, direction));

                UpdateAvatar();
            }
        }

        private void FillForm()
        {
            this.imageEntity = getFirstImageByUserIdUseCase.Execute(new GetFirstImageByUserIdUseCase.Params(userEntity.Id));

            UpdateAvatar();
            FillUserInformation();
        }

        private void FillUserInformation()
        {
            view.SetLogin(userEntity.Login);
            view.SetRole(userEntity.Role.Name);
            view.SetUserId(userEntity.Id);
            view.SetNumber(userEntity.Number);
        }

        private void UpdateAvatar()
        {
            UpdateAccessibilityButtons();

            if (imageEntity != null)
            {
                Bitmap bitmap = convertImageToBitMapUseCase.Execute(new ConvertImageToBitMapUseCase.Params(imageEntity.ImageData));

                if (bitmap != null)
                {
                    view.SetAvatar(bitmap);
                    return;
                }
            }

            view.SetAvatar(Resources.Image);
        }

        private void UpdateAccessibilityButtons()
        {
            view.GetImageNextButton().Enabled = (imageEntity != null && imageEntity.NextNumber != 0);
            view.GetImagePrevButton().Enabled = (imageEntity != null && imageEntity.PrevNumber != 0);
            view.GetUserNextButton().Enabled  = (userEntity.NextNumber != 0);
            view.GetUserPrevButton().Enabled  = (userEntity.PrevNumber != 0);
            view.GetAddImageButton().Enabled  = (userEntity.Id == mainUserId);
        }
    }
}
