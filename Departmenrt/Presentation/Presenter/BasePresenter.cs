﻿using System.Windows.Forms;

namespace Departmenrt.Presentation.Presenter
{
    /// <summary>
    /// Базовый Presenter, предоставляющий основные методы для всех его наследников
    /// </summary>
    public abstract class BasePresenter
    {
        public const string DIALOG_DEFAULT_MESSAGE = "{0}. Вы хотите зарегистрироваться?";

        public DialogResult ShowYesNoErrorMessage(string message)
        {
            return MessageBox.Show(string.Format(DIALOG_DEFAULT_MESSAGE, message), "Ошибка", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
        }

        public void ShowErrorMessage(string message)
        {
            MessageBox.Show(message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
