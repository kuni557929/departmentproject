﻿using Departmenrt.Data;
using Departmenrt.Domain.Data;

namespace Departmenrt.Domain.Interactors
{
    public abstract class BaseUseCase<R, P>
    {
        protected readonly IDepartmentRepository repository;

        public BaseUseCase()
        {
            this.repository = DepartmentRepository.GetInstance();
        }

        public abstract R Execute(P parameters);
    }
}
