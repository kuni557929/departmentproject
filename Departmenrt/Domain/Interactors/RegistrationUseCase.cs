﻿using Departmenrt.Data.Mapper;
using Departmenrt.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace Departmenrt.Domain.Interactors
{
    public class RegistrationUseCase : BaseUseCase<List<Tuple<string, string>>, RegistrationUseCase.Params>
    {
        public override List<Tuple<string, string>> Execute(Params parameters)
        {
            List<Tuple<string, string>> errors = new List<Tuple<string, string>>();

            if (string.IsNullOrEmpty(parameters.Login))
            {
                errors.Add(Tuple.Create("login", "Введите логин!"));
            }

            if (string.IsNullOrEmpty(parameters.Password))
            {
                errors.Add(Tuple.Create("password", "Введите пароль!"));
            }

            if (string.IsNullOrEmpty(parameters.ConfirmPassword))
            {
                errors.Add(Tuple.Create("confirmPassword", "Повторите пароль!"));
            }

            if (Regex.Match(parameters.Login, "[\\W.]").Success)
            {
                errors.Add(Tuple.Create("login", "Введи корректный логин!"));
            }

            if (parameters.Login.Length < 3 || parameters.Login.Length > 20)
            {
                errors.Add(Tuple.Create("login", "Длина логина должна содержать кол-во символов от 3 до 20!"));
            }

            if (!(Regex.IsMatch(parameters.Password, "[A-Z]") && Regex.IsMatch(parameters.Password, "[a-z]") && Regex.IsMatch(parameters.Password, "\\W") && Regex.IsMatch(parameters.Password, "[0-9]")))
            {
                errors.Add(Tuple.Create("password", "Пароль должен минимум содержать одну заглавную букву, одну строчную, один символ и цифру на английском языке!"));
            }

            if (parameters.Password.Length < 8)
            {
                errors.Add(Tuple.Create("password", "Пароль должен быть больше или равно 8 символов!"));
            }

            if (parameters.Password != parameters.ConfirmPassword)
            {
                errors.Add(Tuple.Create("confirmPassword", "Пароли не совпадают!"));
            }

            if (errors.Count == 0)
            {
                if (repository.IsExistsUserByLogin(parameters.Login))
                {
                    throw new UserAlreadyExistsException($"Пользователь с логином: ${parameters.Login} уже существует в системе!");
                }

                repository.SaveUser(UserEntityMapper.toEntity(parameters.Login, DecodePassword(parameters.Password)));
            }

            return errors;
        }

        private string DecodePassword(string password)
        {
            var provider = new SHA1CryptoServiceProvider();
            var encoding = new UnicodeEncoding();
            return Encoding.Default.GetString((provider.ComputeHash(encoding.GetBytes(password))));
        }

        public class Params
        {
            public string Login { get; set; }
            public string Password { get; set; }
            public string ConfirmPassword { get; set; }

            public Params(string login, string password, string confirmPassword)
            {
                this.Login = login;
                this.Password = password;
                this.ConfirmPassword = confirmPassword;
            }
        }
    }
}
