﻿using Departmenrt.Data.Mapper;
using Departmenrt.Domain.Entity;
using System.IO;

namespace Departmenrt.Domain.Interactors
{
    public class SaveImageUseCase : BaseUseCase<ImageEntity, SaveImageUseCase.Params>
    {
        public override ImageEntity Execute(Params parameters)
        {
            byte[] imageData;

            using (FileStream fs = new FileStream(parameters.FileName, FileMode.Open))
            {
                imageData = new byte[fs.Length];
                fs.Read(imageData, 0, imageData.Length);
            }

            string fileName = parameters.FileName;
            string shortFileName = fileName.Substring(fileName.LastIndexOf("/") + 1);

            return repository.SaveImage(ImageEntityMapper.toEntity(shortFileName, fileName, imageData), parameters.UserId);
        }

        public class Params
        {
            public int UserId { get; set; }
            public string FileName { get; set; }

            public Params(string fileName, int userId)
            {
                FileName = fileName;
                UserId = userId;
            }
        }
    }
}
