﻿using Departmenrt.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace Departmenrt.Domain.Interactors
{
    public class AuthorizationUseCase : BaseUseCase<List<Tuple<string, string>>, AuthorizationUseCase.Params>
    {
        public override List<Tuple<string, string>> Execute(Params parameters)
        {
            List<Tuple<string, string>> errors = new List<Tuple<string, string>>();

            if (string.IsNullOrEmpty(parameters.Login))
            {
                errors.Add(Tuple.Create("login", "Введите логин!"));
            }

            if (string.IsNullOrEmpty(parameters.Password))
            {
                errors.Add(Tuple.Create("password", "Введите пароль!"));
            }

            if (Regex.Match(parameters.Login, "[\\W.]").Success)
            {
                errors.Add(Tuple.Create("login", "Введи корректный логин!"));
            }

            if (errors.Count == 0)
            {
                if (repository.IsExistsUserByLogin(parameters.Login))
                {
                    string encodedPassword = repository.GetPasswordByLogin(parameters.Login);

                    if (DecodePassword(parameters.Password) == encodedPassword)
                    {
                        return errors;
                    }

                    throw new InvalidOperationException("Был введен неверный пароль!");
                }
                else
                {
                    throw new UserNotFoundException($"Пользователь с логином: ${parameters.Login} не найден!");
                }
            }

            return errors;
        }

        private string DecodePassword(string password)
        {
            var provider = new SHA1CryptoServiceProvider();
            var encoding = new UnicodeEncoding();
            return Encoding.Default.GetString((provider.ComputeHash(encoding.GetBytes(password))));
        }

        public class Params
        {
            public string Login { get; set; }
            public string Password { get; set; }

            public Params(string login, string password)
            {
                this.Login = login;
                this.Password = password;
            }
        }
    }
}
