﻿using System.Drawing;
using System.IO;

namespace Departmenrt.Domain.Interactors
{
    public class ConvertImageToBitMapUseCase : BaseUseCase<Bitmap, ConvertImageToBitMapUseCase.Params>
    {
        public override Bitmap Execute(Params parameters)
        {
            byte[] bytes = parameters.ImageData;

            using (MemoryStream st = new MemoryStream())
            {
                st.Write(bytes, 0, bytes.Length);
                return new Bitmap(st, false);
            }
        }

        public class Params
        {
            public byte[] ImageData { get; set; }

            public Params(byte[] imageData)
            {
                ImageData = imageData;
            }
        }
    }
}
