﻿using Departmenrt.Domain.Entity;

namespace Departmenrt.Domain.Interactors
{
    public class GetUserByLoginUseCase : BaseUseCase<UserEntity, GetUserByLoginUseCase.Params>
    {
        public override UserEntity Execute(Params parameters)
        {
            return repository.GetUserByLogin(parameters.Login);
        }

        public class Params
        {
            public string Login { get; set; }

            public Params(string login)
            {
                this.Login = login;
            }
        }
    }
}
