﻿using Departmenrt.Data.Enums;
using Departmenrt.Domain.Entity;
using Departmenrt.Domain.Exceptions;
using System;

namespace Departmenrt.Domain.Interactors
{
    public class GetImageByDirectionAndUserIdUseCase : BaseUseCase<ImageEntity, GetImageByDirectionAndUserIdUseCase.Params>
    {
        public override ImageEntity Execute(Params parameters)
        {
            if (parameters.CurrentImage != null)
            {
                long newNumber;

                if (parameters.Direction == Direction.NEXT)
                {
                    newNumber = parameters.CurrentImage.Number + 1;
                }
                else
                {
                    newNumber = parameters.CurrentImage.Number - 1;
                }

                if (newNumber > 0)
                {
                    return repository.GetImageByNumber(parameters.UserId, newNumber);
                }

                throw new ImageNotFoundException($"Неверный номер для картинки ${newNumber} для пользователя с id = ${parameters.UserId}");
            }

            throw new InvalidOperationException("Недопустимое значение переданной картинке: null");
        }

        public class Params
        {
            public int UserId { get; set; }
            public ImageEntity CurrentImage { get; set; }
            public Direction Direction { get; set; }

            public Params (int userId, ImageEntity currentImage, Direction direction)
            {
                UserId = userId;
                CurrentImage = currentImage;
                Direction = direction;
            }
        }
    }
}
