﻿using Departmenrt.Domain.Entity;

namespace Departmenrt.Domain.Interactors
{
    public class GetFirstImageByUserIdUseCase : BaseUseCase<ImageEntity, GetFirstImageByUserIdUseCase.Params>
    {
        public override ImageEntity Execute(Params parameters)
        {
            return repository.GetFirstImageByUserId(parameters.UserId);
        }

        public class Params
        {
            public int UserId { get; set; }

            public Params(int userId)
            {
                UserId = userId;
            }
        }
    }
}
