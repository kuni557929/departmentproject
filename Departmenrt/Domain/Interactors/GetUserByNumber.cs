﻿using Departmenrt.Data.Enums;
using Departmenrt.Domain.Entity;
using Departmenrt.Domain.Exceptions;

namespace Departmenrt.Domain.Interactors
{
    public class GetUserByNumber : BaseUseCase<UserEntity, GetUserByNumber.Params>
    {
        public override UserEntity Execute(Params parameters)
        {
            long number = -1;

            if (parameters.CurrentUser != null)
            {

                if (parameters.Direction == Direction.NEXT)
                {
                    number = parameters.CurrentUser.Number + 1;
                }
                else
                {
                    number = parameters.CurrentUser.Number - 1;
                }

                return repository.GetUserByNumber(number);
            }

            throw new UserNotFoundException($"Пользователь по номеру = {number} не найден!");
        }

        public class Params
        {
            public UserEntity CurrentUser;
            public Direction Direction { get; set; }

            public Params (UserEntity currentUser, Direction direction)
            {
                CurrentUser = currentUser;
                Direction = direction;
            }
        }
    }
}
