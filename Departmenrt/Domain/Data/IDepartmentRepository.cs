﻿using Departmenrt.Domain.Entity;

namespace Departmenrt.Domain.Data
{
    /// <summary>
    /// Интерфейс, содержащий основный методы для работы с базой данных
    /// </summary>
    public interface IDepartmentRepository
    {
        /// <summary>
        /// Метод используется для сохранения пользователя в базу данных
        /// </summary>
        /// <param name="user">Пользователь, который будет сохранен в базу данных</param>
        void SaveUser(UserEntity user);

        /// <summary>
        /// Метод используется для сохранения картинки в базу данных для пользователя с переданным id
        /// </summary>
        /// <param name="image">Картинка, которая будет сохранения и привязана к пользователю</param>
        /// <param name="userId">Id пользователя, к которому нужно привязать картинку</param>
        /// <returns>Сохраненную картинку в базе данных</returns>
        ImageEntity SaveImage(ImageEntity image, int userId);

        /// <summary>
        /// Метод используется для получения недавно добавленной картинки по переданному id пользователя
        /// </summary>
        /// <param name="userId">Id пользователя, у которого будем искать недавно добавленную картинку</param>
        /// <returns>Недавно добавленная картинки или null в случае отстутсвия картинок</returns>
        ImageEntity GetFirstImageByUserId(int userId);

        /// <summary>
        /// Метод используется для получения картинки по порядковому номеру для конкретного пользователя с переданным id
        /// </summary>
        /// <param name="userId">Id пользователя, у которого будем брать картинку по порядковому номеру</param>
        /// <param name="number">Порядковый номер, по которому будем искать картинку</param>
        /// <returns>Найденную картинку или null в случае отсутствия</returns>
        ImageEntity GetImageByNumber(int userId, long number);

        /// <summary>
        /// Метод используется для получения пользователя по логину
        /// </summary>
        /// <param name="login">Логин, по которому будем искать в базе данных пользователя</param>
        /// <returns>Найденного пользователя или null</returns>
        UserEntity GetUserByLogin(string login);

        UserEntity GetUserByNumber(long number);

        /// <summary>
        /// Метод используется для получения пароля по логину
        /// </summary>
        /// <param name="login">Логин, по которому будем брать пароль из базы данных</param>
        /// <returns>Найденный пароль или null в случае его отсутсвия</returns>
        string GetPasswordByLogin(string login);

        /// <summary>
        /// Метод используется для проверки существования пользователя по логину
        /// </summary>
        /// <param name="login">Логин, по которому ищется пользователь в базе данных</param>
        /// <returns>true, если существует, иначе false</returns>
        bool IsExistsUserByLogin(string login);
    }
}
