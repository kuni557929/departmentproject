﻿namespace Departmenrt.Domain.Entity
{
    public class ImageEntity
    {
        public int Id { get; set; }
        public long Number { get; set; }
        public long NextNumber { get; set; }
        public long PrevNumber { get; set; }
        public string Title { get; set; }
        public string FileName { get; set; }
        public byte[] ImageData { get; set; }
    }
}
