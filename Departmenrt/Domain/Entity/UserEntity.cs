﻿namespace Departmenrt.Domain.Entity
{
    public class UserEntity
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public RoleEntity Role { get; set; }
        public long Number { get; set; }
        public long PrevNumber { get; set; }
        public long NextNumber { get; set; }
    }
}
