﻿using System;

namespace Departmenrt.Domain.Exceptions
{
    public class ImageNotFoundException : Exception
    {
        public ImageNotFoundException(string message) : base(message) { }
    }
}
