﻿using System;

namespace Departmenrt.Domain.Exceptions
{
    public class ImageNotExistsException : Exception
    {
        public ImageNotExistsException(string message) : base(message)
        {

        }
    }
}
