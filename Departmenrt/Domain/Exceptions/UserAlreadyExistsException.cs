﻿using System;
namespace Departmenrt.Domain.Exceptions
{
    public class UserAlreadyExistsException : Exception
    {
        public UserAlreadyExistsException(string message) : base(message)
        {
            
        } 
    }
}
