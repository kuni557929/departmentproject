﻿using Departmenrt.Domain.Entity;

namespace Departmenrt.Data.Mapper
{
    public class RoleEntityMapper
    {
        public static RoleEntity toEntity(int id, string name)
        {
            RoleEntity role = new RoleEntity();
            role.Id = id;
            role.Name = name;
            return role;
        }
    }
}
