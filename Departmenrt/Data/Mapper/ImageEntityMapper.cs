﻿using Departmenrt.Domain.Entity;

namespace Departmenrt.Data.Mapper
{
    public static class ImageEntityMapper
    {
        public static ImageEntity toEntity(int id, string title, string fileName, byte[] imageData, long number, long nextNumber, long prevNumber)
        {
            ImageEntity imageEntity = new ImageEntity();
            imageEntity.Id = id;
            imageEntity.Number = number;
            imageEntity.Title = title;
            imageEntity.FileName = fileName;
            imageEntity.ImageData = imageData;
            imageEntity.NextNumber = nextNumber;
            imageEntity.PrevNumber = prevNumber;
            return imageEntity;
        }

        public static ImageEntity toEntity(int id, string title, string fileName, byte[] imageData, long number)
        {
            ImageEntity imageEntity = new ImageEntity();
            imageEntity.Id = id;
            imageEntity.Number = number;    
            imageEntity.Title = title;
            imageEntity.FileName = fileName;
            imageEntity.ImageData = imageData;
            return imageEntity;
        }

        public static ImageEntity toEntity(string title, string fileName, byte[] imageData)
        {
            ImageEntity imageEntity = new ImageEntity();
            imageEntity.Title = title;
            imageEntity.FileName = fileName;
            imageEntity.ImageData = imageData;
            return imageEntity;
        }
    }
}
