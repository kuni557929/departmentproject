﻿using Departmenrt.Domain.Entity;

namespace Departmenrt.Data.Mapper
{
    public class UserEntityMapper
    {

        public static UserEntity toEntity(string login, string password)
        {
            RoleEntity roleEntity = new RoleEntity();
            roleEntity.Id = 4;
            UserEntity userEntity = new UserEntity();
            userEntity.Login = login;
            userEntity.Password = password;
            userEntity.Role = roleEntity;
            return userEntity;
        }

        public static UserEntity toEntity(int id, string login, string password, RoleEntity role, long number, long prevNumber, long nextNumber)
        {
            UserEntity userEntity = new UserEntity();
            userEntity.Role = role;
            userEntity.Id = id;
            userEntity.Login = login;
            userEntity.Password = password;
            userEntity.Number = number;
            userEntity.PrevNumber = prevNumber;
            userEntity.NextNumber = nextNumber;
            return userEntity;
        }
    }
}
