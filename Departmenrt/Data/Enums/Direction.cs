﻿namespace Departmenrt.Data.Enums
{
    public enum Direction
    {
        NEXT = 1,
        PREV = 2
    }
}
