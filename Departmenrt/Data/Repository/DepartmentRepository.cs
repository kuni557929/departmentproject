﻿using Departmenrt.Data.Mapper;
using Departmenrt.Domain.Data;
using Departmenrt.Domain.Entity;
using System;
using System.Data.SqlClient;

namespace Departmenrt.Data
{
    /// <summary>
    /// Реализация <see cref="cref="Departmenrt.Domain.Data.IDepartmentRepository"/>
    /// </summary>
    public class DepartmentRepository : IDepartmentRepository
    {
        public const string SQL_CONNECTION_STRING = "Server=MRBOMBA\\SQLEXPRESS;Database=department;Trusted_Connection=True;";
        public const string SQL_INSERT_INTO_USERS = "INSERT INTO users (login, password, role_id) VALUES (@login, @password, @role_id);";
        public const string SQL_SAVE_IMAGE = "INSERT INTO images (title, filename, image_data, user_id) OUTPUT INSERTED.id VALUES (@title, @filename, @image_data, @user_id);";
        public const string SQL_GET_FIRST_IMAGE_BY_USER_ID = "SELECT TOP(1) t.id, t.title, t.filename, t.image_data, ISNULL(number, 0) AS number, ISNULL(LEAD(number) OVER(ORDER BY number), 0) AS next_number, ISNULL(LAG(number) OVER(ORDER BY number), 0) AS prev_number FROM (SELECT id, title, filename, image_data, ROW_NUMBER() OVER(ORDER BY i.id DESC) AS number FROM images i WHERE user_id = @user_id) t;";
        public const string SQL_GET_IMAGE_BY_ID = "SELECT id, title, filename, image_data, ISNULL(ROW_NUMBER() OVER(ORDER BY id), 0) AS number, 0, 0 FROM images WHERE user_id = @user_id AND id = @image_id;";
        public const string SQL_GET_IMAGE_BY_NUMBER = "SELECT v.id, v.title, v.filename, v.image_data, v.number, v.next_number, v.prev_number FROM (SELECT t.id, t.title, t.filename, t.image_data, ISNULL(number, 0) AS number, ISNULL(LEAD(number) OVER(ORDER BY number), 0) AS next_number, ISNULL(LAG(number) OVER(ORDER BY number), 0) AS prev_number FROM (SELECT id, title, filename, image_data, ROW_NUMBER() OVER(ORDER BY i.id DESC) AS number FROM images i WHERE user_id = @user_id) t) v WHERE v.number = @number;";
        public const string SQL_GET_USER_BY_LOGIN = "SELECT * FROM (SELECT v.id, v.login, v.password, v.role_id, v.role_name, v.number, ISNULL(LAG(v.number) OVER(ORDER BY v.id), 0) AS prev_number, ISNULL(LEAD(v.number) OVER(ORDER BY v.id), 0) AS next_number FROM (SELECT u.id, u.login, u.password, r.id AS role_id, r.name_role AS role_name, ROW_NUMBER() OVER(ORDER BY u.id) AS number FROM users u INNER JOIN roles r ON u.role_id = r.id) v) p WHERE p.login = @login;";
        public const string SQL_GET_USER_BY_NUMBER = "SELECT * FROM (SELECT v.id, v.login, v.password, v.role_id, v.role_name, v.number, ISNULL(LAG(v.number) OVER(ORDER BY v.id), 0) AS prev_number, ISNULL(LEAD(v.number) OVER(ORDER BY v.id), 0) AS next_number FROM (SELECT u.id, u.login, u.password, r.id AS role_id, r.name_role AS role_name, ROW_NUMBER() OVER(ORDER BY u.id) AS number FROM users u INNER JOIN roles r ON u.role_id = r.id) v) p WHERE p.number = @number";
        public const string SQL_GET_PASSWORD_BY_LOGIN = "SELECT password FROM users u WHERE u.login = @login;";
        public const string SQL_IS_EXISTS_USER_BY_LOGIN = "SELECT 1 FROM users u WHERE u.login = @login;";

        private static DepartmentRepository instance;

        private readonly SqlConnection connection;

        public DepartmentRepository()
        {
            connection = new SqlConnection(SQL_CONNECTION_STRING);
            connection.Open();
        }

        ~DepartmentRepository()
        {
            try
            {
                connection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Произошла ошибка при закрытии обьекта SqlConnection: ${ex.Message}");
            }
        }

        public static DepartmentRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new DepartmentRepository();
            }

            return instance;
        }

        public void SaveUser(UserEntity user)
        {
            SqlCommand cmd = GenerateSqlCommand(SQL_INSERT_INTO_USERS);
            cmd.Parameters.AddWithValue("login", user.Login);
            cmd.Parameters.AddWithValue("password", user.Password);
            cmd.Parameters.AddWithValue("role_id", user.Role.Id);
            cmd.ExecuteNonQuery();
        }

        public ImageEntity SaveImage(ImageEntity image, int userId)
        {
            SqlCommand cmd = GenerateSqlCommand(SQL_SAVE_IMAGE);
            cmd.Parameters.AddWithValue("title", image.Title);
            cmd.Parameters.AddWithValue("filename", image.FileName);
            cmd.Parameters.AddWithValue("image_data", image.ImageData);
            cmd.Parameters.AddWithValue("user_id", userId);
            int imageId = (int) cmd.ExecuteScalar();

            return GetImage(SQL_GET_IMAGE_BY_ID, new SqlParameter[] { new SqlParameter("user_id", userId), new SqlParameter("image_id", imageId) });
        }

        public ImageEntity GetFirstImageByUserId(int userId)
        {
            return GetImage(SQL_GET_FIRST_IMAGE_BY_USER_ID, new SqlParameter[] { new SqlParameter("user_id", userId) });
        }

        public ImageEntity GetImageByNumber(int userId, long number)
        {
            return GetImage(SQL_GET_IMAGE_BY_NUMBER, new SqlParameter[] { new SqlParameter("user_id", userId), new SqlParameter("number", number) });
        }

        public UserEntity GetUserByLogin(string login)
        {
            SqlCommand cmd = GenerateSqlCommand(SQL_GET_USER_BY_LOGIN);
            cmd.Parameters.AddWithValue("login", login);

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    RoleEntity role = RoleEntityMapper.toEntity(reader.GetInt32(3), reader.GetString(4));
                    return UserEntityMapper.toEntity(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), role, reader.GetInt64(5), reader.GetInt64(6), reader.GetInt64(7));
                }
            }

            return null;
        }

        public UserEntity GetUserByNumber(long number)
        {
            SqlCommand cmd = GenerateSqlCommand(SQL_GET_USER_BY_NUMBER);
            cmd.Parameters.AddWithValue("number", number);

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    RoleEntity role = RoleEntityMapper.toEntity(reader.GetInt32(3), reader.GetString(4));
                    return UserEntityMapper.toEntity(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), role, reader.GetInt64(5), reader.GetInt64(6), reader.GetInt64(7));
                }
            }

            return null;
        }

        public string GetPasswordByLogin(string login)
        {
            SqlCommand cmd = GenerateSqlCommand(SQL_GET_PASSWORD_BY_LOGIN);
            cmd.Parameters.AddWithValue("login", login);

            return cmd.ExecuteScalar().ToString();
        }

        public bool IsExistsUserByLogin(string login)
        {
            SqlCommand cmd = GenerateSqlCommand(SQL_IS_EXISTS_USER_BY_LOGIN);
            cmd.Parameters.AddWithValue("login", login);

            return cmd.ExecuteScalar() != null;
        }

        private ImageEntity GetImage(string sql, SqlParameter[] parameters)
        {
            SqlCommand cmd = GenerateSqlCommand(sql);
            cmd.Parameters.AddRange(parameters);

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    return ImageEntityMapper.toEntity(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), (byte[])reader.GetValue(3), reader.GetInt64(4), reader.GetInt64(5), reader.GetInt64(6));
                }
            }

            return null;
        }

        private SqlCommand GenerateSqlCommand(string query)
        {
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandText = query;
            return cmd;
        }
    }
}
